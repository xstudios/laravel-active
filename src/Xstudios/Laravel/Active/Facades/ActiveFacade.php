<?php namespace Xstudios\Laravel\Active\Facades;

use Illuminate\Support\Facades\Facade;

class ActiveFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'active';
    }
}
