<?php namespace Xstudios\Laravel\Active;

use Illuminate\Support\ServiceProvider;
use Xstudios\Laravel\Active\Active;

class ActiveServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('active', function($app)
		{
			return new Active($app['request'], $app['router']);
		});
	}

	/**
	 * Boot the service provider.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('xstudios/laravel-active');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('active');
	}

}
