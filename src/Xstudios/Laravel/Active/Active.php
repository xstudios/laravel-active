<?php namespace Xstudios\Laravel\Active;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

class Active {

    /**
     * Illuminate Request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Illuminate Router instance.
     *
     * @var \Illuminate\Routing\Router
     */
    protected $router;

    public function __construct(Request $request, Router $router)
    {
        $this->request = $request;
        $this->router = $router;
    }

    /**
     * Return active class if paths are matched.
     *
     * @param  string | array  $paths
     * @param  string  $class
     * @return mixed
     */
    /*public function path($paths, $class = 'active')
    {
        return $this->request->is($paths) ? $class : null;
    }*/

    /**
     * Return active class if route names are matched.
     *
     * @param  string | array  $routes
     * @param  string  $class
     * @return mixed
     */
    public function route($routes, $class = 'active')
    {
        $resource_routes = array('index', 'create', 'edit', 'destroy', 'show');
        $test_routes = array();

        // Create known resource routes
        if (is_array($routes))
        {
            foreach ($routes as $_r)
            {
                // Add in the route as is
                array_push($test_routes, $_r);

                // Add in the route with known resource postfixex
                foreach ($resource_routes as $_route)
                {
                     array_push($test_routes, $_r.'.'.$_route);
                }
            }
        }
        else
        {
            // Add in the route as is
            array_push($test_routes, $routes);

            // Add in the route with known resource postfixex
            foreach ($resource_routes as $_route)
            {
                 array_push($test_routes, $routes.'.'.$_route);
            }
        }

        // Test each route to see if we have a match
        foreach ($test_routes as $_route)
        {
            if ($this->router->is($_route))
            {
                return $class;
            }
        }

        return null;
    }

}
