# Laravel Active

## Overview
Laravel 4 package for easily adding CSS active states.

## Installation
Open your `composer.json` file and add the following to the `repositories` array:

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/xstudios/laravel-active.git"
        }
    ],

The add the folllowing to the `require` object:

    "xstudios/laravel-active": "v0.1.2"

Or to stay on the bleeding edge (stability not guaranteed):

    "xstudios/laravel-active": "dev-master"

### Install the dependencies

    php composer install

or

    php composer update

Once the package is installed you need to register the service provider with the application. Open up `app/config/app.php` and find the `providers` key.

    'providers' => array(
        # Other providers here ...
        'Xstudios\Laravel\Active\ActiveServiceProvider',
    )

And finally add this to the `aliases` key.

    'aliases' => array(
        # Other aliases here ...
        'Active' => 'Xstudios\Laravel\Active\Facades\ActiveFacade'
    }

## Usage
By passing in the base route, Active will automatically check for common `resource` routes (index, create, edit, destroy, show):

    // Base route of 'admin.post' will check for routes:
    // admin.post.index, admin.post.create, admin.post.edit, admin.post.destroy, and admin.post.show
    Active::route('admin.post'); // active

An array version works also.

    // Base routes of 'admin.post' and 'admin.category' will check for routes:
    // admin.post.index, admin.post.create, admin.post.edit, admin.post.destroy, and admin.post.show
    // admin.category.index, admin.category.create, admin.category.edit, admin.category.destroy, and admin.category.show
    Active::route(array('admin.post', 'admin.category')); // active

Pass in full route name.

    Active::route('admin.post.view'); // active

Pass in custom classes.

    Active::route('admin.post', 'myactivestate'); // myactivestate

Blade template example:

    <li class="{{ Active::route('admin.posts') }}">
        <a href="{{ URL::route('admin.posts.index') }}">Posts</a>
    </li>

## Contribute
In lieu of a formal styleguide, take care to maintain the existing coding style.

## License
MIT License (c) [X Studios](http://xstudiosinc.com)
